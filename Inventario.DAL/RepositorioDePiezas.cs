﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventario.DAL
{
    public class RepositorioDePiezas : IRepositorio1<Piezas>
    {
        private string DBname = @"C:\INVENTARIOS\Inventario.db";
        private string TableName = "piezas";
        public List<Piezas> Read
        {
            get
            {
                List<Piezas> datos = new List<Piezas>();
                using (var db = new LiteDatabase(DBname))
                {
                    datos = db.GetCollection<Piezas>(TableName).FindAll().ToList();
                }
                return datos;
            }

        }

        public bool Create(Piezas entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            try
            {
                using (var db = new LiteDatabase(DBname))
                {
                    var coleccion = db.GetCollection<Piezas>(TableName); coleccion.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(string Id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBname))
                {
                    var coleccion = db.GetCollection<Piezas>(TableName); r = coleccion.Delete(e => e.Id == Id);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Piezas EntidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBname))
                {
                    var coleccion = db.GetCollection<Piezas>(TableName); coleccion.Update(EntidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}


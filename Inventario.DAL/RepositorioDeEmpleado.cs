﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventario.DAL
{
    public class RepositorioDeEmpleados : IRepositorio1<Empleado>
    {
        private string DBname = @"C:\INVENTARIOS\Inventario.db";
        private string TableName = "Empleados";
        public List<Empleado> Read
        {
            get
            {
                List<Empleado> datos = new List<Empleado>();
                using (var db = new LiteDatabase(DBname))
                {
                    datos = db.GetCollection<Empleado>(TableName).FindAll().ToList();
                }
                return datos;
            }
        }
        public bool Create(Empleado entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            try
            {
                using (var db = new LiteDatabase(DBname))
                {
                    var coleccion = db.GetCollection<Empleado>(TableName); coleccion.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(string Id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBname))
                {
                    var coleccion = db.GetCollection<Empleado>(TableName); r = coleccion.Delete(e => e.Id == Id);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Empleado EntidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBname))
                {
                    var coleccion = db.GetCollection<Empleado>(TableName); coleccion.Update(EntidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}



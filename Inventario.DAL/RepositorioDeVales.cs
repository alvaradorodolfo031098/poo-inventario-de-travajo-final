﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventario.DAL
{
    public class RepositorioDeVales : IRepositorio1<Vale>
    {
        private string DBname = @"C:\INVENTARIOS\Inventario.db";
        private string TableName = "Vale";
        public List<Vale> Read
        {
            get
            {
                List<Vale> datos = new List<Vale>();
                using (var db = new LiteDatabase(DBname))
                {
                    datos = db.GetCollection<Vale>(TableName).FindAll().ToList();
                }
                return datos;
            }
        }

        public bool Create(Vale entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            try
            {
                using (var db = new LiteDatabase(DBname))
                {
                    var coleccion = db.GetCollection<Vale>(TableName); coleccion.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(string Id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBname))
                {
                    var coleccion = db.GetCollection<Vale>(TableName); r = coleccion.Delete(e => e.Id == Id);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Vale EntidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBname))
                {
                    var coleccion = db.GetCollection<Vale>(TableName); coleccion.Update(EntidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

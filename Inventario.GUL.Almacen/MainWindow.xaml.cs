﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inventario.BIZ;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Inventario.COMMON.Interfaces;
using Inventario.DAL;
using Inventario.COMMON.Entidades;

namespace Inventario.GUL.Almacen
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }

        ManejadorDeVales manejadorDeVales;
        ManejadorEmpleados manejadoeDeEmpleado;
        ManejadorPiezas manejadorPieza;
        Vale vale;

        accion accionVales;

            public MainWindow()
        {
            InitializeComponent();
            manejadorDeVales = new ManejadorDeVales(new RepositorioDeVales());
            manejadoeDeEmpleado = new ManejadorEmpleados(new RepositorioDeEmpleados());
            manejadorPieza = new ManejadorPiezas(new RepositorioDePiezas());

            ActualizarTablaDeVales();
            GridDetalle.IsEnabled = false;
        }

        private void ActualizarTablaDeVales()
        {
            dtgVales.ItemsSource = null;
            dtgVales.ItemsSource = manejadorDeVales.Listar;
        }

        private void btnNuevoVale_Click(object sender, RoutedEventArgs e)
        {
            GridDetalle.IsEnabled = true;
            ActualizarComboDetalle();
            vale = new Vale();
            vale.PiezasPrestados = new List<Piezas>();
            ActualizarListaDePiezasEnVale();
            accionVales = accion.Nuevo;
        }

        private void ActualizarListaDePiezasEnVale()
        {
            dtgPiezasEnVale.ItemsSource = null;
            dtgPiezasEnVale.ItemsSource = vale.PiezasPrestados;
        }

        private void ActualizarComboDetalle()
        {
            cmbAlmacenista.ItemsSource = null;
            cmbAlmacenista.ItemsSource = manejadoeDeEmpleado.EmpleadosPorArea("Almacen");

            cmbPiezas.ItemsSource = null;
            cmbPiezas.ItemsSource = manejadorPieza.Listar;

            cmbSolicitante.ItemsSource = null;
            cmbSolicitante.ItemsSource = manejadoeDeEmpleado.Listar;
        }

        private void btnEliminarVale_Click(object sender, RoutedEventArgs e)
        {
            Vale v = dtgVales.SelectedItem as Vale;
            if(v != null)
            {
                if(MessageBox.Show("Realmente deseas eliminar este vale?", "Almacen", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorDeVales.Eliminar(v.Id))
                    {
                        MessageBox.Show("Eliminado con exito", "Almacen", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        ActualizarTablaDeVales();
                    }
                    else
                    {
                        MessageBox.Show("Algo salio mal", "Almacen", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void btnAgregarPieza_Click(object sender, RoutedEventArgs e)
        {
            Piezas m = cmbPiezas.SelectedItem as Piezas;
            if (m != null)
            {
                vale.PiezasPrestados.Add(m);
                ActualizarListaDePiezasEnVale();
                
            }
        }

        private void btnEliminarPieza_Click(object sender, RoutedEventArgs e)
        {
            Piezas m = dtgPiezasEnVale.SelectedItem as Piezas;
            if (m != null)
            {
                vale.PiezasPrestados.Remove(m);
                ActualizarListaDePiezasEnVale();
            }
        }

        private void btnGuradarVale_Click(object sender, RoutedEventArgs e)
        {
            if (accionVales == accion.Nuevo)
            {
                vale.EncargadoDeEntrega = cmbAlmacenista.SelectedItem as Empleado;
                vale.FechaEntrega = dtpFechaEntrega.SelectedDate.Value;
                vale.FechaHoraSolicitud = DateTime.Now;
                vale.Solicitante = cmbSolicitante.SelectedItem as Empleado;
                if (manejadorDeVales.Agregar(vale))
                {
                    MessageBox.Show("Vale guardado con exito", "Almacen", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    LimpiarCamposDeVales();
                    GridDetalle.IsEnabled = false;
                    ActualizarTablaDeVales();
                }
                else
                {
                    MessageBox.Show("Error al guardar el vale", "Almacen", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                vale.EncargadoDeEntrega = cmbAlmacenista.SelectedItem as Empleado;
                vale.FechaEntrega = dtpFechaEntrega.SelectedDate.Value;
                vale.FechaEntregaReal = DateTime.Parse(lblFechaHoraEnteregaReal.Content.ToString());
                vale.FechaHoraSolicitud = DateTime.Now;
                vale.Solicitante = cmbSolicitante.SelectedItem as Empleado;
                if (manejadorDeVales.Modificar(vale))
                {
                    MessageBox.Show("Vale guardado con exito", "Almacen", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    LimpiarCamposDeVales();
                    GridDetalle.IsEnabled = false;
                    ActualizarTablaDeVales();
                }
                else
                {
                    MessageBox.Show("Error al guardar el vale", "Almacen", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void LimpiarCamposDeVales()
        {
            dtpFechaEntrega.SelectedDate = DateTime.Now;
            lblFechaHoraEnteregaReal.Content="";
            lblFechaHoraPrestamo.Content = "";
            dtgPiezasEnVale.ItemsSource = null;
            cmbAlmacenista.ItemsSource = null;
            cmbPiezas.ItemsSource = null;
            cmbSolicitante.ItemsSource = null;
        }

        private void dtgVales_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Vale v = dtgVales.SelectedItem as Vale;
            if (v != null)
            {
                GridDetalle.IsEnabled = true;
                vale = v;
                ActualizarListaDePiezasEnVale();
                accionVales = accion.Editar;
                lblFechaHoraPrestamo.Content = vale.FechaHoraSolicitud.ToString();
                lblFechaHoraEnteregaReal.Content = vale.FechaEntregaReal.ToString();
                ActualizarComboDetalle();
                cmbAlmacenista.Text = vale.EncargadoDeEntrega.ToString();
                cmbSolicitante.Text = vale.Solicitante.ToString();
                dtpFechaEntrega.SelectedDate = vale.FechaEntrega;
            }
        }

        private void btnEntregarVale_Click(object sender, RoutedEventArgs e)
        {
            lblFechaHoraEnteregaReal.Content = DateTime.Now;
            
        }

        private void btnCancelarVale_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeVales();
            GridDetalle.IsEnabled = false;
        }

        private void btnReportes_Click(object sender, RoutedEventArgs e)
        {
            Reportes reportes = new Reportes();
            reportes.Show();
        }
    }
}

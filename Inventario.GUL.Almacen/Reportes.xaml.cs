﻿using Inventario.BIZ;
using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using Inventario.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Reporting.WinForms;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Inventario.GUL.Almacen;
using Inventario.COMMON.Modelos;

namespace Inventario.GUL.Almacen
{
    /// <summary>
    /// Lógica de interacción para Reportes.xaml
    /// </summary>
    public partial class Reportes : Window
    {
        IManejadorEmpleados manejadorEmpleados;
        IManejadorDeVales manejadorVales;
        public Reportes()
        {
            InitializeComponent();
            manejadorEmpleados=new ManejadorEmpleados(new RepositorioDeEmpleados());
            manejadorVales = new ManejadorDeVales(new RepositorioDeVales());
            cmbPersona.ItemsSource = manejadorEmpleados.Listar;
        }

        private void cmbPersona_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(cmbPersona.SelectedItem != null)
            {
                dtgTablaVales.ItemsSource = null;
                dtgTablaVales.ItemsSource= manejadorVales.BuscarNoEmtregadoPorEmpleado(cmbPersona.SelectedItem as Empleado);
            }
        }

        private void btnImprimirPorPersona_Click(object sender, RoutedEventArgs e)
        {
            List<ReportDataSource> datos = new List<ReportDataSource>();
            ReportDataSource vale = new ReportDataSource();
            List<ModelValesParaReportes> valesPorEntregar =new List<ModelValesParaReportes>();
            foreach(var item in manejadorVales.ValesPorLiquidar())
            {
                valesPorEntregar.Add(new ModelValesParaReportes(item));
            }
            vale.Value = valesPorEntregar;
            vale.Name = "Vales";
            datos.Add(vale);
            Respoteador ventana = new Respoteador("Inventario.GUL.Almacen.Reportes.SinParametros.rdlc", datos);
            ventana.ShowDialog();
        }
    }
}

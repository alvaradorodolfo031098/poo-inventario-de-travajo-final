﻿using Inventario.BIZ;
using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using Inventario.DAL;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Inventario.GUL.Administrador
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        IManejadorEmpleados manejadorEmpleados;
        IManejadorPiezas manejadorPiezas;

        accion accionEmpleados;
        accion accionPiezas;
        public MainWindow()
        {
            InitializeComponent();
            manejadorEmpleados = new ManejadorEmpleados(new RepositorioDeEmpleados());
            manejadorPiezas = new ManejadorPiezas(new RepositorioDePiezas());

            PonerBotonesEmpleadoEnEdicion(false);
            LimpiarCamposDeEmpleados();
            ActualizarTablaEmpleados();

            PonerBotonesDePiezasEnEdicion(false);
            LimpiarCamposDePiezas();
            ActualizarTablaDePiezas();
        }

        private void ActualizarTablaDePiezas()
        {
            dtgPiezas.ItemsSource = null;
            dtgPiezas.ItemsSource = manejadorPiezas.Listar;
        }

        private void LimpiarCamposDePiezas()
        {
            txtPiezaCategoria.Clear();
            txtPiezaDescripcion.Clear();
            txbPiezasId.Text = "";
            txtPiezaNombre.Clear();
        }

        private void PonerBotonesDePiezasEnEdicion(bool value)
        {
            btnPiezasCancelar.IsEnabled = value;
            btnPiezasEditar.IsEnabled = !value;
            btnPiezasEliminar.IsEnabled = !value;
            btnPiezasGuardar.IsEnabled = value;
            btnPiezasNuevo.IsEnabled = !value;
        }

        private void ActualizarTablaEmpleados()
        {
            dtgEmpleado.ItemsSource = null;
            dtgEmpleado.ItemsSource = manejadorEmpleados.Listar;
        }

        private void LimpiarCamposDeEmpleados()
        {
            txbEmpleadosId.Text = "";
            txtEmpleadoApellido.Clear();
            txtEmpleadoArea.Clear();
            txtEmpleadoNombre.Clear();
        }

        private void PonerBotonesEmpleadoEnEdicion(bool value)
        {
            btnEmpleadosCancelar.IsEnabled = value;
            btnEmpleadosEditar.IsEnabled = !value;
            btnEmpleadosEditar.IsEnabled = !value;
            btnEmpleadosGuardar.IsEnabled = value;
            btnEmpleadosNuevo.IsEnabled = !value;
        }

        private void btnEmpleadosNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEmpleados();
            PonerBotonesEmpleadoEnEdicion(true);
            accionEmpleados = accion.Nuevo;
        }

        private void btnEmpleadosEditar_Click(object sender, RoutedEventArgs e)
        {
            Empleado emp = dtgEmpleado.SelectedItem as Empleado;
            if (emp != null)
            {
                txbEmpleadosId.Text = emp.Id;
                txtEmpleadoApellido.Text = emp.Apellido;
                txtEmpleadoArea.Text = emp.Area;
                txtEmpleadoNombre.Text = emp.Nombre;
                accionEmpleados = accion.Editar;
                PonerBotonesEmpleadoEnEdicion(true);
            }

        }

        private void btnEmpleadosGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionEmpleados == accion.Nuevo)
            {
                Empleado emp = new Empleado()
                {
                    Nombre = txtEmpleadoNombre.Text,
                    Apellido = txtEmpleadoApellido.Text,
                    Area = txtEmpleadoArea.Text

                };
                if (manejadorEmpleados.Agregar(emp))
                {
                    MessageBox.Show("Empleado agregado corectamente", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeEmpleados();
                    ActualizarTablaEmpleados();
                    PonerBotonesEmpleadoEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El empleado no se pudo agregar ", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Empleado emp = dtgEmpleado.SelectedItem as Empleado;
                emp.Apellido = txtEmpleadoApellido.Text;
                emp.Area = txtEmpleadoArea.Text;
                emp.Nombre = txtEmpleadoNombre.Text;
                if (manejadorEmpleados.Modificar(emp))
                {
                    MessageBox.Show("Empleado modificado corectamente", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeEmpleados();
                    ActualizarTablaEmpleados();
                    PonerBotonesEmpleadoEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El empleado no se pudo actualizar ", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }


        private void btnEmpleadosCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEmpleados();
            PonerBotonesEmpleadoEnEdicion(false);
        }

        private void btnEmpleadosEliminar_Click(object sender, RoutedEventArgs e)
        {
            Empleado emp = dtgEmpleado.SelectedItem as Empleado;
            if (emp != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este empleado?", "Inventarios", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorEmpleados.Eliminar(emp.Id))
                    {
                        MessageBox.Show("Empleado eliminado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaEmpleados();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el empleado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnPiezasNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDePiezas();
            accionPiezas = accion.Nuevo;
            PonerBotonesDePiezasEnEdicion(true);
        }

        private void btnPiezasEditar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDePiezas();
            accionPiezas = accion.Editar;
            PonerBotonesDePiezasEnEdicion(true);
            Piezas m = dtgPiezas.SelectedItem as Piezas;
            if (m != null)
            {
                txtPiezaCategoria.Text = m.Categoria;
                txtPiezaDescripcion.Text = m.Descripcion;
                txbPiezasId.Text = m.Id;
                txtPiezaNombre.Text = m.Nombre;
                imgFoto.Source = ByteToImage(m.Fotografia);
            }
        }

        public ImageSource ByteToImage(byte[] imageData)
        {
            if (imageData == null)
            {
                return null;
            }
            else
            {
                BitmapImage biImg = new BitmapImage();
                MemoryStream ms = new MemoryStream(imageData);
                biImg.BeginInit();
                biImg.StreamSource = ms;
                biImg.EndInit();
                ImageSource imgSrc = biImg as ImageSource;
                return imgSrc;
            }
        }

        private void btnPiezasGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionPiezas == accion.Nuevo)
            {
                Piezas m = new Piezas()
                {
                    Categoria = txtPiezaCategoria.Text,
                    Descripcion = txtPiezaDescripcion.Text,
                    Nombre = txtPiezaNombre.Text,
                    Fotografia = ImageToByte(imgFoto.Source)
                };
                if (manejadorPiezas.Agregar(m))
                {
                    MessageBox.Show("Pieza correctamente agregada", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDePiezas();
                    ActualizarTablaDePiezas();
                    PonerBotonesDePiezasEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("Algo salio mal ", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Piezas m = dtgPiezas.SelectedItem as Piezas;
                m.Categoria = txtPiezaCategoria.Text;
                m.Descripcion = txtPiezaDescripcion.Text;
                m.Nombre = txtPiezaNombre.Text;
                m.Fotografia = ImageToByte(imgFoto.Source);
                if (manejadorPiezas.Modificar(m))
                {
                    MessageBox.Show("Pieza corectamente Modificado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDePiezas();
                    ActualizarTablaDePiezas();
                    PonerBotonesDePiezasEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("Algo salio mal ", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        public byte[] ImageToByte(ImageSource image)
        {
            if (image != null)
            {
                MemoryStream menstrean = new MemoryStream();
                JpegBitmapEncoder enconder = new JpegBitmapEncoder();
                enconder.Frames.Add(BitmapFrame.Create(image as BitmapSource));
                enconder.Save(menstrean);
                return menstrean.ToArray();
            }
            else
            {
                return null;
            }
        }

        private void btnPiezasCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDePiezas();
            PonerBotonesDePiezasEnEdicion(false);
        }

        private void btnPiezasEliminar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDePiezas();
            PonerBotonesDePiezasEnEdicion(false);
            Piezas m = dtgPiezas.SelectedItem as Piezas;
            if (MessageBox.Show("Realmente deseas eliminar esta pieza?", "Inventarios", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (manejadorPiezas.Eliminar(m.Id))
                {
                    MessageBox.Show("Piezas eliminado corectamente", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTablaDePiezas();
                }
                else
                {
                    MessageBox.Show("No se pudo eliminar las piezas", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void CagarFoto_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog diagolo = new OpenFileDialog();
            diagolo.Title = "Selecionar una foto";
            diagolo.Filter = "Archivo de imagen| *.jpg;*.pnj;.gif";
            if (diagolo.ShowDialog().Value)
            {
                imgFoto.Source = new BitmapImage(new Uri(diagolo.FileName));
            }
        }

        
    }
}






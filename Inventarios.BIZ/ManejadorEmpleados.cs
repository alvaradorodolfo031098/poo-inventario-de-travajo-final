﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Inventario.BIZ
{
    public class ManejadorEmpleados : IManejadorEmpleados
    {
        IRepositorio1<Empleado> repositorio;
        public ManejadorEmpleados(IRepositorio1<Empleado> repo)
        {
            repositorio = repo;
        }

        public List<Empleado> Listar => repositorio.Read.OrderBy(p=>p.Nombre).ToList();


        public bool Agregar(Empleado Entidad)

        {
            return repositorio.Create(Entidad);
        }
                      
        public Empleado BuscarPorId(string Id)
        {
            return Listar.Where(e => e.Id == Id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return repositorio.Delete(id);
        }

        public List<Empleado> EmpleadosPorArea(string area)
        {
            return Listar.Where(e => e.Area == area).ToList();
        }

        public bool Modificar(Empleado entidad)
        {
            return repositorio.Update(entidad);
        }
    }
}
 

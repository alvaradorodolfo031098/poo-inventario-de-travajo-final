﻿using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventario.BIZ
{
    public class ManejadorPiezas : IManejadorPiezas
    {
        IRepositorio1<Piezas> repositorio;
        public ManejadorPiezas(IRepositorio1<Piezas> repositorio)
        {
            this.repositorio = repositorio;
        }

        public List<Piezas> Listar => repositorio.Read;


        public bool Agregar(Piezas Entidad)

        {
            return repositorio.Create(Entidad);
        }

        public Piezas BuscarPorId(string Id)
        {
            return Listar.Where(e => e.Id == Id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return repositorio.Delete(id);
        }

        public bool Modificar(Piezas entidad)
        {
            return repositorio.Update(entidad);
        }

        public List<Piezas> PiezasPorCategoria(string Categoria)
        {
            return Listar.Where(e => e.Categoria == Categoria).ToList();
        }
    }
}


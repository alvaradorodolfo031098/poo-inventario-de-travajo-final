﻿using Inventario.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventario.COMMON.Modelos
{
   public class ModelValesParaReportes
    {
        public Vale vale { get; set; }
        public string NombreSolicitante { get; set; }
        public string NombreAlmacenista { get; set; }
        public DateTime Solicitud { get; set; }
        public DateTime Entrega { get; set; }
        public int Cantidad { get; set; }
        public ModelValesParaReportes(Vale vale)
            
        {
            this.vale = vale;
            NombreSolicitante = String.Format("{0}{1}", vale.Solicitante.Nombre, vale.Solicitante.Apellido);
            NombreAlmacenista = string.Format("{0}{1}", vale.EncargadoDeEntrega.Nombre, vale.EncargadoDeEntrega.Apellido);
            Solicitud = vale.FechaHoraSolicitud;
            Entrega = vale.FechaEntrega;
            Cantidad = vale.PiezasPrestados.Count;
        }
    }
}

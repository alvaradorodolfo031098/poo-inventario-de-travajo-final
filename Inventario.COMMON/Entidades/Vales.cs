﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventario.COMMON.Entidades
{
    public class Vale : Base
    {
        public DateTime FechaHoraSolicitud { get; set; }
        public DateTime FechaEntrega { get; set; }
        public DateTime? FechaEntregaReal { get; set; }
        public List<Piezas> PiezasPrestados { get; set; }
        public Empleado Solicitante { get; set; }
        public Empleado EncargadoDeEntrega { get; set; }

        public override string ToString()
        {
            return String.Format("{0} {1} deve {2} articulos y los deveria de entregar el {3}", Solicitante.Nombre, Solicitante.Apellido, PiezasPrestados.Count, FechaEntrega.ToShortDateString());
        }
    }
}